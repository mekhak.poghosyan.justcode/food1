import {createStore, combineReducers, applyMiddleware} from 'redux';
import thunk from "redux-thunk" 
import ChangePasswordReducer from './reducers/changePasswrodReducer';
import RegisterReducer from './reducers/regiseterRefucer';

const rootReducer = combineReducers({
  register: RegisterReducer,
  changePassword:ChangePasswordReducer,
});

export const store = createStore(rootReducer, applyMiddleware(thunk) );
