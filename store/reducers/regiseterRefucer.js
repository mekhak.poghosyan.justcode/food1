const initialState = {
  success: '',
  error:{},
  code:'',
  statusMail:'',
  mail:''
};

const RegisterReducer = (state = initialState, action) => {
  let item = {...state}
  switch (action.type) {
    case 'SuccessRegister':
      item.success = true
      break;
    case 'errorRegister':
     item.error = action.data
      break
    case 'changeStatus':
      item.success = ''
      item.error = {}
      item.statusMail ='',
      item.code = ''
    break
    case 'SuccesVereficationMail':
      item.code = action.data
      item.mail = action.email
      break
    case 'succesCode':
      item.statusMail = true
      break
    case 'succesResendCode': 
      item.code = action.data
    default:
      break
  }
  return item
};
export default RegisterReducer;
