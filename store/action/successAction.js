export const SuccessRegister = () => {
    return {
        type:'SuccessRegister'
    }
}

export const SuccesVereficationMail = (data,email) => {
    console.log(email)
    return {
        type:'SuccesVereficationMail',
        data,
        email,
    }
}
export const succesCode = () =>{
    return {
        type:'succesCode'
    }
}
export const succesResendCode = (data) =>{
    return {
        type:'succesResendCode',
        data
    }
}

export const succesSendMailChangePassword = () =>{
    return {
        type:'succesSendMailChangePassword'
    }
}