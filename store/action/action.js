import axios from "axios"
import { ErrorChangePassword, ErrorRegister } from "./errorAction"
import { SuccessRegister,SuccesVereficationMail,succesCode, succesResendCode, succesSendMailChangePassword } from "./successAction"

export const RegisterAction = (data) => {
    return (dispatch) => {
        axios.post('https://food.justcode.am/api/RegisterIndividual',data).then((e)=>{
            if(e.data.status){
                dispatch(SuccessRegister())
            }
            else {
                dispatch(ErrorRegister(e.data))
            }
        })
    }
}
export const changeStatus = () =>{
    return {
        type:'changeStatus',
    }
}

export const sendMail = (email) => {
    return (dispatch) => {
        axios.post('https://food.justcode.am/api/ResendCodeFromRegisterEmail',email).then((e)=>{
            if(e.data.status){
                dispatch(SuccesVereficationMail(e.data.code,email.email))
            }
        })
    }
}

export const sendCode = (code) => {
    return (dispatch) =>{
        axios.post('https://food.justcode.am/api/VerificationOfVerificationCode',code).then((e)=>{
            console.log(e.data)
            if(e.data.status){
                dispatch(succesCode())
            }
        })
    }
}

export const ResendCode = (email) =>{
    console.log(email)
    return (dispatch) =>{
        axios.post('https://food.justcode.am/api/ResendCodeFromRegisterEmail',{email}).then((e)=>{
            console.log(e.data)
            dispatch(succesResendCode(e.data.code))
        })
    }
}

export const chnagePassword = (email) => {
    console.log(email)
    return (dispatch) =>{
        axios.post('https://food.justcode.am/api/SendCodeFromEmail',email).then((e)=>{
            if(e.data.status){
                dispatch(succesSendMailChangePassword())
            }
            else {
                dispatch(ErrorChangePassword(e.data.email))
            }
            console.log(e.data.code)
        })
    }
}



// https://food.justcode.am/api/documentation#/ResetPassword