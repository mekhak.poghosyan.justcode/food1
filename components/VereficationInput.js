import { TextInput, View,StyleSheet } from "react-native"

export default VereficationInput = ({value,onChangeText}) => {
    return <View>
        <TextInput value={value} onChangeText ={onChangeText} style = {styles.input}></TextInput>
    </View>
}

const styles = StyleSheet.create({
    input:{
        height:60,
        width:45,
        borderColor:'#3C6954',
        borderWidth:1,
        borderRadius:8,
        marginHorizontal:5,
        fontSize:20,
        textAlign:'center'
    }
})