import {Text,StyleSheet, TouchableOpacity} from 'react-native';

export default Button = ({title,width = 270,height =50 , noFill,onPress,radius =10 }) => {
  return (
    <TouchableOpacity 
      style={[styles.Btn,noFill?styles.Btn1:styles.Btn2,{width:width,height:height,borderRadius:radius}]} 
      onPress={onPress}
    >
      <Text style = {[styles.txt,noFill ? styles.txt1:styles.txt2]}>{title}</Text>
    </TouchableOpacity>
  );
};


const styles = StyleSheet.create({
    Btn:{
        justifyContent:'center',
        marginBottom:20,
    },
    txt:{
      textAlign:'center',
      fontFamily:'Raleway',
      fontStyle:'normal',
      lineHeight:21,
      fontSize:18,
    },
    txt1:{
      color:'white'
    },
    txt2:{
      fontWeight:700,
      color:'#000',
    },  
    Btn1:{
      backgroundColor:'#3C6954',
    },
    Btn2:{
      backgroundColor:'#F5F5F5',
      borderWidth:2,
      borderColor:'#3C6954'
    },
  });