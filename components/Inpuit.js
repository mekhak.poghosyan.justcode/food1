import { Text, TextInput, View,StyleSheet } from "react-native"

export const Input = ({lable,placeholder,value,onChangeText,error,errText}) => {
    return <View >
        <Text style = {styles.Lable} >{lable}</Text>
        <TextInput onChangeText={onChangeText} value = {value} style = {[styles.Input,(error ||errText) && styles.error]} placeholder = {placeholder} />
        {errText &&
            <Text style = {styles.errText} >
            {
                 errText
            }
        </Text>}
    </View>
}

const styles = StyleSheet.create({
    Input:{
        borderWidth:1,
        borderColor:'#333333',
        width:325,
        height:40,
        borderRadius:6,
        paddingHorizontal:10,
        marginBottom:20,
        color:'#000',
    },
    Lable:{
        fontSize:15,
        fontWeight:400,
        marginBottom:5,
        color:'#333333'
    },
    error:{
        borderColor:'red'
    },
    errText:{
        marginTop:-20,
        color:'red',
        fontSize:12,
        marginBottom:10
    }
});