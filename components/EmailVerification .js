import { useEffect, useState } from "react"
import { View,Text, StyleSheet, TouchableOpacity, Modal } from "react-native"
import { useDispatch, useSelector } from "react-redux"
import { Gstyles } from "../Gstyles"
import Button from "./Button"
import  VereficationInput  from "./VereficationInput"
import { changeStatus, ResendCode, sendCode } from "../store/action/action"
import PopUp from "./popUp"

export default EmailVerification = ({navigation}) =>{
    const [openPopUp,setOpenPopUp] = useState(false)
    const dispatch = useDispatch()
        const {register} = useSelector(r=>r)
        const [code,setCode] = useState([
        {value:'',index:1},
        {value:'',index:2},
        {value:'',index:3},
        {value:'',index:4},
        {value:'',index:5},
        {value:'',index:6},
    ])
    const handleClick = () =>{
        dispatch(changeStatus())
        navigation.goBack()
        setOpenPopUp(false)
   }
    const check = () =>{
       let value = ''
       code.map((elm)=>{
        value+=elm.value 
       }) 
       if(value == register.code){
            dispatch(sendCode({email:register.mail,code:register.code}))
       }
    }
        const handelChange = (e,index) =>{
        let item = [...code]
        item[index].value = e
        setCode(item)
    }
    useEffect(()=>{
        if(register.statusMail){
            setOpenPopUp(true)
            // dispatch(changeStatus())
            // navigation.replace('Login')
        }
    },[register.statusMail])
    return <View style = {[Gstyles.Container,styles.Container]} >
        <Modal  backdropOpacity={10}  backdropColor={"rgba(255, 0, 0, 0.8)"}  transparent={true} navigation ={navigation} visible={openPopUp} >
            <PopUp onPress = {()=>handleClick()} text ={'Ваш аккаунт на модерации'} />
        </Modal>
        <View>
             <Text style  ={Gstyles.Title2}>Подтверждение Эл. Почты</Text>
             <Text style  ={[Gstyles.text,styles.text]}>На вашу эл. почту отправлен код подтверждения,введите его ниже чтобы закончить регистрацию</Text>
         </View>
         <View style = {styles.input}>
            <View  style = {styles.inputWrapper} >
             {code.map((elm,index)=>(
                 <VereficationInput 
                    value = {elm.value}  
                    key={index} 
                    onChangeText = {(e)=>handelChange(e,index)} 
                />
             ))
             }
             </View>
             <View style = {[Gstyles.Url,styles.url]}  >
                <TouchableOpacity onPress = {()=>dispatch(ResendCode({email:register.mail}))}>
                    <Text style = {Gstyles.Link}> Отправить код повторно </Text>
                </TouchableOpacity>
             </View>
        </View>
        <View>
            <Button onPress={()=>check()}   title={'Подтвердить'} noFill = {true}  />
        </View>
    </View>
}
const styles =  StyleSheet.create({
    input:{
        marginBottom:40,
    },
    inputWrapper:{
        flexDirection:'row',
    },
    text:{
        padding:30,
    },
    resetCode:{
        fontWeight: 500,
        fontSize:14,
        textAlign:'center',
        color:"#333333",
        borderBottomWidth:1,
        borderBottomColor:'#333333',
        width:325,
    },
    url:{
        marginVertical:10,
    },
    Container:{
        marginTop:0,
    }
})

