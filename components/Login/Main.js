import {SafeAreaView} from 'react-native';
import {Gstyles} from '../../Gstyles';
import HeadLogin from './HeadLogin';

export default Main = ({navigation}) => {
  return (
    <SafeAreaView style={Gstyles.Main}>
      <HeadLogin
        title="Добро пожаловать"
        text={'Регистрирутесь или зайдите в уже существующий аккаунт'}
        ButtonTitle={'Войти'}
        ButtonPress={ ()=>navigation.navigate('Login')}
        ButtonPress1={ ()=>navigation.navigate('Roll')}
        ButtonTitle1={'Зарегистрироватся'}
      />
    </SafeAreaView>
  );
};
