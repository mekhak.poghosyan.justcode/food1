import { Text, View,StyleSheet } from 'react-native';
import Button from '../Button'

export default HeadLogin = ({title, text, ButtonTitle, ButtonTitle1,ButtonPress,ButtonPress1}) => {
  return (
    <View style ={styles.container}>
      <View style = {styles.textContainer}>
        <Text style = {styles.Title}>{title}</Text>
        <Text style = {styles.Text}>{text}</Text>
      </View>
      <View  style = {styles.BtnContainer}>
        <Button onPress = {ButtonPress} title={ButtonTitle} noFill = {true}  />
        <Button onPress = {ButtonPress1} title={ButtonTitle1} noFill = {false} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent: 'center',
        alignItems:'center',
    },
    textContainer:{
        alignItems:'center',
    },
    Title:{
        fontSize:34,
        color:'#333333',
        fontWeight: 600,
        marginBottom:10,
    },
    Text:{
        width:280,
        textAlign:'center',
        fontWeight: 400,
        fontSize:17,
        color:'#000'
    },    
    BtnContainer:{
        marginTop:100,
    },
    
  });