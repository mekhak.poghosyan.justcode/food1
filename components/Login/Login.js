import { StyleSheet,   View ,Text } from "react-native"
import { Gstyles } from "../../Gstyles"
import Button from "../Button"
import { Input } from "../Inpuit"

export default Login = ({navigation}) => {
    return <View style = {[Gstyles.Container]}>
        <Text style = {Gstyles.Title2} >Вход</Text>
        <View>
            <Input lable = {'Эл. почта'} />
            <Input lable = {'Пароль'} placeholder = 'Пароль' />
        </View>
        <View  style = {styles.Url}>
            <Text style = {styles.Link} onPress={() =>navigation.navigate('RecoveryPassword')}>Забыли пароль?</Text>
        </View>
        <View style = {styles.ButtonProvider} >
            <Button  title={'Войти'} noFill = {true}  />
        </View>
    </View>
}

const styles =  StyleSheet.create({
    Url: {
        width:325,
        justifyContent:'flex-end',
        alignItems:'flex-end',
        marginTop:-10
    },
    Link:{
        fontSize:15,
        fontWeight:500,
        borderBottomWidth:1,
        borderBottomColor:'#333333',
    },
    ButtonProvider:{
        marginTop:40,
    }   
})