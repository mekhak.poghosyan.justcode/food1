import { View,Text, StyleSheet } from "react-native"
import { Gstyles } from "../Gstyles"
import Button from "./Button"
import { Input } from "./Inpuit"

export const NewPassword = () => {
    return <View style = {[Gstyles.Container]} >
        <View>
            <Text style ={[Gstyles.Title2,styles.title]}>Задайте новый пароль</Text>
            <Text style ={[Gstyles.text,styles.text]}>
                Придумайте сложный пароль,содержащий
                строчные и прописные буквы,а так же цифры
                и символы
            </Text>
        </View>
        <View style = {styles.inputWrapper}>
            <Input lable={'Новый пароль'}/>
            <Input lable={'Новый пароль'} />
        </View>
        <View>
            <Button title={'Подтвердить'} noFill = {true} />
        </View>
    </View>
}
const styles = StyleSheet.create({
    title:{
        padding:20,
    },
    text:{
        padding:20,
        textAlign:'left'
    },
    inputWrapper:{
        marginBottom:10,
    }
})