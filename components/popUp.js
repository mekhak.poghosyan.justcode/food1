import { StyleSheet, Text, View } from "react-native"
import { Path, Svg } from "react-native-svg"
import { useDispatch } from "react-redux"
import Button from "./Button"

export default PopUp = ({onPress ,text}) => {
    dispatch = useDispatch()
   
    return <View style = {style.popUp} >
        <View style = {style.popUpWrapper} >
        <View>
        <Svg
            width={66}
            height={66}
            viewBox="0 0 66 66"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
            >
            <Path
                d="M33 65.375A32.374 32.374 0 1133 .626a32.374 32.374 0 010 64.749zM33 5.25a27.75 27.75 0 100 55.5 27.75 27.75 0 000-55.5z"
                fill="#3C6954"
            />
            <Path
                d="M43.614 46.875L30.688 33.948v-21.76h4.625v19.84l11.562 11.586-3.26 3.261z"
                fill="#3C6954"
            />
        </Svg>
        </View>
        <View style = {style.textWrapper}>
            <Text style = {style.text}>{text}</Text>
        </View>
        <Button onPress={onPress} title={'Закрыть'} noFill ={true}></Button>
        </View>
    </View>
}
const style = StyleSheet.create({
    popUp:{
        justifyContent:'center',
        alignItems:'center',
        flex:1,
    },
    textWrapper:{
        marginVertical:30,
    },
    popUpWrapper:{
        width:335,
        height:370,
        backgroundColor: '#FFFFFF',
        borderRadius:20,
        justifyContent:'center',
        alignItems:'center',
    },
    text:{
        fontSize:28,
        fontWeight:600,
        textAlign:'center',
        color: '#333333',
    }
})