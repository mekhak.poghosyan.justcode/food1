import { useEffect, useState } from "react"
import { SafeAreaView, ScrollView,  StyleSheet,Text, View } from "react-native"
import { Path, Rect, Svg } from "react-native-svg"
import { useDispatch, useSelector } from "react-redux"
import { Gstyles } from "../Gstyles"
import { changeStatus, RegisterAction, sendMail } from "../store/action/action"
import * as EmailValidator from 'email-validator';
import Button from "./Button"
import { Input } from "./Inpuit"

export default Register = ({route,navigation}) => {
    const register = useSelector(r=>r.register)
    const dispatch = useDispatch()

    
    const {role} = route?.params
    const [fiz,setFiz]  = useState([
        {title:'ИНН',value:'',error:''},
        {title:'Имя',value:'',error:''},
        {title:'Фамилия',value:'',error:''},
        {title:'Отчество',value:'',error:''},
        {title:'Город',value:'',error:''},
        {title:'Индекс',value:'',error:''},
        {title:'Телефон',value:'',error:''},
        {title:'Эл. почта',value:'',error:'',title2:'email'},
        {title:'Пароль',value:'',error:'',title2:'password',},
        {title:'Повтор пароля',value:'',error:'',title2:'password_confirmation',},
    ])
    const [isSelected, setSelection] = useState(false);
    const [switchButtons,setSwitchButtons] = useState(false)
    const handleChange = (value,i) => {
        let item = [...fiz]
        item[i].value = value
        setFiz(item)
    }
    const Validation = () => {
        let item =[...fiz]
       if(!switchButtons){
        item = item.filter(function(value, index, arr){ 
            return value.title !== 'ИНН';
        });
       }
        let error  = false
        item.map((elm,index)=>{
            if(elm.value ===''){
                item[index].error = 'empty'
            }
            else {
                item[index].error = ''
            }
            if(elm.title === 'Эл. почта' && elm.value !== '') {
                if(!EmailValidator.validate("test@email.com")){
                    item[index].error = 'mail error'
                }
                else {
                    item[index].error = ''
                }
            }
            if(elm.title === 'Повтор пароля'){
                if(item[index-1].value !==elm.value){
                   elm.error = 'password'
             }
               else {
                 elm.error = ''
               }
            }
        })
        item.map((elm,i)=>{
            if(elm.error !== ''){
                error = true
            }
        })
        if(!error && isSelected){
            if(!switchButtons){
                var data = {
                    name: item[0].value,
                    surname: item[1].value,
                    patronymic: item[0].value,
                    city: item[3].value,
                    CityIndex:  item[4].value,
                    phone:  item[5].value,
                    email: item[6].value,
                    password:item[7].value,
                    password_confirmation:item[8].value,
                    SellerorBuyer: role,
                    RoleId: switchButtons+1,
                  }
            }
            else {
                var data = {
                    name: item[1].value,
                    surname: item[2].value,
                    patronymic: item[1].value,
                    city: item[4].value,
                    CityIndex:  item[5].value,
                    phone:  item[6].value,
                    email: item[7].value,
                    password:item[8].value,
                    password_confirmation:item[9].value,
                    SellerorBuyer: role,
                    RoleId: switchButtons+1,
                  }
            }
              
            dispatch(RegisterAction(data))
        }
        setFiz(item)
    }
    useEffect(()=>{
        if(register.success){
            if(!switchButtons){
                dispatch(sendMail({email:fiz[6].value}))
            }
            else {
                dispatch(sendMail({email:fiz[7].value}))
            }
            navigation.replace('EmailVerification')
            dispatch(changeStatus())
        }
    },[register.success])
    return (
        <SafeAreaView style = {Gstyles.Container}>
            <View style = {styles.BtnProvider}>
                <Button  radius = {5} width={160} height = {29} onPress = {()=>setSwitchButtons(false)} title={'Физ. лицо'} noFill = {!switchButtons}  />
                <Button  radius = {5} width={160} height = {29} onPress = {()=>setSwitchButtons(true)} title={'Юр. лицо'}  noFill = {switchButtons} />
            </View>
            <ScrollView showsVerticalScrollIndicator={false} style={{paddingHorizontal: 20, marginTop: 20}}>
                {fiz.map((elm,i)=>{
                if(!switchButtons && i !==0){
                    return <Input 
                        error = {elm.error !== '' }  
                        value ={elm.value} 
                        onChangeText = {(e)=>handleChange(e,i)} 
                        lable = {elm.title}  
                        key = {i}
                         errText = {register.error[elm.title2]}
                    />  
                }
                else if (switchButtons){
                    return <Input 
                    error = {elm.error !== '' } 
                    value ={elm.value } 
                    onChangeText = {(e)=>handleChange(e,i)} 
                    lable = {elm.title}  key = {i}
                    errText = {register.error[elm.title2]}
                    />  
                }
                })}
            <View style = {styles.CheckBox}>
           { !isSelected ?<Svg onPress={()=>setSelection(true)}
                width={25}
                height={25}
                viewBox="0 0 25 25"
                fill="none" 
                xmlns="http://www.w3.org/2000/svg"
                >
                <Rect
                    x={0.75}
                    y={0.75}
                    width={23.5}
                    height={23.5}
                    rx={4.25}
                    stroke="#3C6954"
                    strokeWidth={1.5}
                />
            </Svg>:
            <Svg  onPress={()=>setSelection(false)}
                width={25}
                height={25}
                viewBox="0 0 24 24"
                xmlns="http://www.w3.org/2000/svg"
                >
                <Path
                    d="M18.25 3A2.75 2.75 0 0121 5.75v12.5A2.75 2.75 0 0118.25 21H5.75A2.75 2.75 0 013 18.25V5.75A2.75 2.75 0 015.75 3h12.5zm0 1.5H5.75c-.69 0-1.25.56-1.25 1.25v12.5c0 .69.56 1.25 1.25 1.25h12.5c.69 0 1.25-.56 1.25-1.25V5.75c0-.69-.56-1.25-1.25-1.25zM10 14.44l6.47-6.47a.75.75 0 011.133.976l-.073.084-7 7a.75.75 0 01-.976.073l-.084-.073-3-3a.75.75 0 01.976-1.133l.084.073L10 14.44l6.47-6.47L10 14.44z"
                    fill="#212121"
                    fillRule="nonzero"
                    stroke="none"
                    strokeWidth={1}
                />
            </Svg> 
            }
                <Text style = {styles.txt}>Согласие на обработку персональных данных.</Text>
            </View>
           <View style = {styles.Btn} >
               <Button  onPress={()=>Validation()}  title={'Зарегистрироваться'} noFill = {true}  />
           </View>
            </ScrollView>
        </SafeAreaView>
    )
}
const styles =  StyleSheet.create({
    BtnProvider:{
        flex: 1,
        flexDirection:'row',
        justifyContent:'space-between',
        width:325,
        marginVertical: 20,
    },
    Btn:{
        alignItems:'center',
        marginTop:30
    },
    CheckBox:{
        flexDirection:'row',
        alignItems:'center',
    },
    txt:{
        fontSize:12,
        marginLeft:10,
        color:'#333333',
        fontWeight:400,
    },
  
})