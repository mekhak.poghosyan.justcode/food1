import { StyleSheet, Text, View } from "react-native"
import { Path, Svg } from "react-native-svg"
import { Gstyles } from "../Gstyles"
import Button from "./Button"

export const SuccesChangePassword = ({navigation}) => {
    return <View style = {[Gstyles.Container]}>
        <View>
            <Svg
                width={62}
                height={54}
                viewBox="0 0 62 54"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
                >
                <Path
                    d="M57.626 0L20.36 44.366 4.542 24.669 0 30.02l10.246 12.068L20.36 54 30.41 42.164 62 4.954 57.626 0z"
                    fill="#333"
            />
            </Svg>
        </View>
        <View style = {style.text1}>
            <Text style ={style.text} >Ваш пароль</Text>
            <Text style ={style.text} >успешно изменён</Text>
        </View>
        <View>
            <Button onPress={()=>navigation.navigate('Login')} title={'Войти'} noFill ={true} />
        </View>
    </View>
}

const style = StyleSheet.create({
    text:{
        fontSize:30,
        fontWeight:300,
        color:'#333333',
        textAlign:'center',
    },
    text1:{
        marginVertical:100,
    }
})