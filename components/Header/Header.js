import {StyleSheet, Text, View} from 'react-native';
import { Svg,Path} from 'react-native-svg';

export const Header = ({onPress,title}) => {
  return (
    <View style={styles.Header}>
      <View style={[styles.HeaderView]}>
          <Svg
            onPress={onPress}
            width={11}
            height={21}
            viewBox="0 0 11 21"
            fill="none"
            xmlns="http://www.w3.org/2000/svg">
            <Path
              d="M9.169 20.708a1.457 1.457 0 01-1.138-.54l-7.043-8.75a1.459 1.459 0 010-1.851L8.279.817a1.46 1.46 0 012.246 1.866L4.006 10.5l6.3 7.817a1.458 1.458 0 01-1.137 2.391z"
              fill="#3C6954"
            />
          </Svg>
        <Text style={styles.HeaderText}>{title}</Text>
        <Text style={styles.HeaderText}> </Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  Header: {
    backgroundColor: '#F5F5F5',
    height:80,
  },
  HeaderView: {
    marginTop:20,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginLeft:30,
  },
  HeaderText: {
    fontSize: 36,
    color: '#333333',
    fontWeight: '500',
  },
});
