import Button from "./Button"
import { useState } from "react"
import { View,Text, StyleSheet, TouchableOpacity,} from "react-native"
import { Gstyles } from "../Gstyles"
import  VereficationInput  from "./VereficationInput"

export default RecoverPasswordVerefication = () =>{
    const [code,setCode] = useState([
        {value:'',index:1},
        {value:'',index:2},
        {value:'',index:3},
        {value:'',index:4},
        {value:'',index:5},
        {value:'',index:6},
    ])
    return <View style = {[Gstyles.Container,styles.Container]} >
    <View>
         <Text style  ={Gstyles.Title2}>Восстановление пароля</Text>
         <Text style  ={[Gstyles.text,styles.text]}>Мы отправим 6-ти значный код на вашу эл. почту для подтверждения личности</Text>
     </View>
     <View style = {styles.input}>
        <View  style = {styles.inputWrapper} >
         {code.map((elm,index)=>(
             <VereficationInput 
                value = {elm.value}  
                key={index} 
                onChangeText = {(e)=>handelChange(e,index)} 
            />
         ))
         }
         </View>
         <View style = {[Gstyles.Url,styles.url]}  >
            <TouchableOpacity >
                <Text style = {Gstyles.Link}> Отправить код повторно </Text>
            </TouchableOpacity>
         </View>
    </View>
    <View>
        <Button title={'Подтвердить'} noFill = {true}  />
    </View>
</View>
}
const styles =  StyleSheet.create({
input:{
    marginBottom:40,
},
inputWrapper:{
    flexDirection:'row',
},
text:{
    padding:30,
},
resetCode:{
    fontWeight: 500,
    fontSize:14,
    textAlign:'center',
    color:"#333333",
    borderBottomWidth:1,
    borderBottomColor:'#333333',
    width:325,
},
url:{
    marginVertical:10,
},
Container:{
    marginTop:0,
}
})
