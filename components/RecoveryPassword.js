import { useEffect, useState } from "react";
import { StyleSheet, Text, View } from "react-native"
import { useDispatch, useSelector } from "react-redux";
import { Gstyles } from "../Gstyles"
import { chnagePassword } from "../store/action/action";
import Button from "./Button";
import { Input } from "./Inpuit";

export default RecoveryPassword = ({navigation}) => {
    const dispatch = useDispatch()
    const {changePassword} = useSelector(r=>r)
    useEffect(()=>{ 
        if(changePassword.status){
            navigation.navigate('RecoverPasswordVerefication')
        }
    },[changePassword.status])
    const [email,seteMail] = useState()
    return <View   style = {[Gstyles.Container]}>
        <View>
            <Text style = {styles.title}>Восстановление пароля </Text>
            <Text  style = {styles.txt}>Мы отправим 6-ти значный код на вашу эл. почту для подтверждения личности</Text>
        </View>
        <View style = {styles.inputWraper}>
            <Input errText={changePassword.error} value={email} onChangeText = {(e)=>seteMail(e)} lable = 'Эл. почта' />
        </View>
        <View>
            <Button title='Отправить код' onPress={()=>dispatch(chnagePassword({email:email}))} noFill = {true} />
        </View>
    </View>
}
const styles = StyleSheet.create({
    title:{
        fontSize:36,
        fontWeight:600,
        color: '#333333',
        textAlign: 'center',
    },
    txt:{
        fontWeight:400,
        fontSize:14,
        textAlign:"center",
        color: '#545454',
        marginVertical:20,
    },
    inputWraper:{
        marginBottom:100,
    }
});