import { SafeAreaView} from 'react-native';
import { Gstyles } from '../Gstyles';
import HeadLogin from './Login/HeadLogin';

export default Roll = ({navigation}) => {
  return (
    <SafeAreaView style = {Gstyles.Main}>
      <HeadLogin 
            title = 'Выбор роли'
            text = 'Выберите пожалуйста роль регистрации аккаунта'  
            ButtonTitle = 'Продавец'
            ButtonTitle1 = 'Покупатель'
            ButtonPress={ ()=>navigation.navigate('Register',{role:'Seller'})}
            ButtonPress1={ ()=>navigation.navigate('Register',{role:'Buyer'})}
        />
    </SafeAreaView>
  );
};
