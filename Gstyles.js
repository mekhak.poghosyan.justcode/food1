import {StyleSheet} from 'react-native';

export const Gstyles = StyleSheet.create({
    Main:{
      flex:1,
      backgroundColor:'#F5F5F5'
    },
    Container:{
      flex: 1,
      justifyContent:'center',
      alignItems:'center',
  },
  Title:{
    fontSize:34,
    color:'#333333',
    fontWeight: 600,
    marginBottom:10,
  },
  Title2:{
    textAlign:'center',
    fontSize:36,
    color:'#333333',
    marginBottom:10,
    fontWeight: 600,
  },
  text:{
    fontSize:14,
    fontWeight: 400,
    textAlign:'center',
    color: '#545454',
  },
  Url: {
    width:325,
    justifyContent:'flex-end',
    alignItems:'center',
},
Link:{
  fontSize:15,
  fontWeight:500,
  borderBottomWidth:1,
  borderBottomColor:'#333333',
},
  });