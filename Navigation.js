import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import roll from './components/Roll';
import Main from './components/Login/Main';
import Login from './components/Login/Login';
import Register from './components/Register';
import { Header } from './components/Header/Header';
import RecoveryPassword from './components/RecoveryPassword';
import EmailVerification from './components/EmailVerification ';
import RecoverPasswordVerefication from './components/RecoverPasswordVerefication';
import { NewPassword } from './components/newPassword';
import { SuccesChangePassword } from './components/succesChangePassword';

const Stack = createNativeStackNavigator();

export default function MyStack() {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName="Main"
        // screenOptions={{headerShown: false}}
        >
        <Stack.Screen name="Main" component={Main}
        options={
          {
            title:' Main Screen', 
            headerShown: false
          }
          
          }/>
        <Stack.Screen name="Roll" component={roll} options={{
          title:' Roll Screen',
          header: ({navigation}) => <Header onPress ={()=>navigation.goBack()}  />
      }}/>
        <Stack.Screen name="Login" component={Login} options={{
          title:' Login Screen',
          header: ({navigation}) => <Header  onPress ={()=>navigation.goBack()}  />
          }}/>
        <Stack.Screen
          name="Register"
          component={Register}
          options={{
            title: ' Register Screen',
            header: ({navigation}) => <Header onPress ={()=>navigation.goBack()} title={'Регистрация'} height = {80}/>
          }}
        />
        <Stack.Screen
          name="RecoveryPassword"
          component={RecoveryPassword}
          options={{
            title: ' RecoveryPassword Screen',
            header: ({navigation}) => <Header onPress ={()=>navigation.goBack()}  height = {80}/>
          }}
        />
        <Stack.Screen
          name="EmailVerification"
          component={EmailVerification}
          options={{
            title: ' EmailVerification Screen',
            header: ({navigation}) => <Header onPress ={()=>navigation.goBack()} title={''} height = {80}/>
          }}
        />
        <Stack.Screen
          name="RecoverPasswordVerefication"
          component={RecoverPasswordVerefication}
          options={{
            title: ' RecoverPasswordVerefication Screen',
            header: ({navigation}) => <Header onPress ={()=>navigation.goBack()} title={''} height = {80}/>
          }}
        />
        <Stack.Screen
          name="NewPassword"
          component={NewPassword}
          options={{
            title: ' NewPassword Screen',
            header: ({navigation}) => <Header onPress ={()=>navigation.goBack()} title={''} height = {80}/>
          }}
        />
        <Stack.Screen
          name="SuccesChangePassword"
          component={SuccesChangePassword}
          options={{
            title: ' SuccesChangePassword Screen',
            header: ({navigation}) => <Header onPress ={()=>navigation.goBack()} title={''} height = {80}/>
          }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}