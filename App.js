import React from 'react';

import MyStack from './Navigation';
import { Provider } from 'react-redux';
import 'react-native-gesture-handler';
import { store } from './store/configStore';
export default function App() {
  return(
    <Provider store={store}>
      <MyStack />
    </Provider>
  )
}
